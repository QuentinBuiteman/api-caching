# API Caching
Cache your API requests, for a faster reload on second try. Uses `localStorage` to save it.

## Installation
Using NPM:

```
npm install api-caching --save
```

Using Yarn:

```
yarn add api-caching
```

## How to use
Import the library and use it's `fetch` function.

```js
import API from 'api-caching';

const url = '/your-endpoint';
const options = {
  lifeTime: 7 * 24 * 60 * 60,
  refreshTimestamp: Date.now(),
};

// Build the API request
const request = new API(url, options);

request
  .fetch()
  .then((response) => {
    // Do something with your response

    // Save to cache
    promise.cachePut(response);
  });
```

### Options
You can also pass an optional second parameter to the `API` object. This must be an object, which can have one of the following keys:

#### lifeTime
A response is saved for a month. Overwrite this by setting an expiration time in seconds.

#### refreshTimestamp
If this timestamp is newer than the timestamp when the response is cached, the cached response will not be retrieved, but instead fetched again.

#### body
Data to use for a POST request. Make sure this is a json object. If body is supplied, method will be POST, else it will be GET.
