class Cache {
  /**
   * Build the cache object
   *
   * @param {string} url - URL to use for cache
   *
   * @returns {void}
   */
  constructor(url) {
    this.url = url;
    this.cacheLifeTime = 4 * 7 * 24 * 60 * 60;
    this.cacheRefreshTimestamp = 0;
  }

  /**
   * Get the lifetime
   *
   * @returns {int}
   */
  get lifeTime() {
    return this.cacheLifeTime;
  }

  /**
   * Get the refreshTimestamp
   *
   * @returns {int}
   */
  get refreshTimestamp() {
    return this.cacheRefreshTimestamp;
  }

  /**
   * Set the lifetime
   *
   * @param {int} time - Time in seconds to use as lifetime of cache
   *
   * @returns {void}
   */
  set lifeTime(time) {
    if (time) {
      this.cacheLifeTime = time;
    }
  }

  /**
   * Set the lifetime
   *
   * @param {int} timestamp - Timestamp to save as refreshTimestamp
   *
   * @returns {void}
   */
  set refreshTimestamp(timestamp) {
    if (timestamp) {
      this.cacheRefreshTimestamp = timestamp;
    }
  }

  /**
   * Retrieve the cache data for the URL
   *
   * @returns {mixed}
   */
  get() {
    const cache = JSON.parse(localStorage.getItem(this.url));

    if (cache !== null) {
      const currentTimestamp = new Date().getTime() / 1000;
      const { expireTimestamp, setTimestamp } = cache;

      // If current is newer than expireTimestamp, don't get cached result
      if (currentTimestamp > expireTimestamp) {
        return false;
      }

      // If refreshed time is newer than setTimestamp, don't get cached result
      if (this.refreshTimestamp > setTimestamp) {
        return false;
      }

      return cache.data;
    }

    return false;
  }

  /**
   * Set the cache data for the URL
   *
   * @param {object} json - Data to save in cache
   *
   * @returns {void}
   */
  put(json) {
    // Build cache object
    const setTimestamp = new Date().getTime() / 1000;
    const cache = {
      setTimestamp,
      expireTimestamp: setTimestamp + this.lifeTime,
      data: json,
    };

    // Set in localStorage
    localStorage.setItem(this.url, JSON.stringify(cache));
  }
}

export default Cache;
