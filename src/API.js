import fetch from 'isomorphic-fetch';
import 'abortcontroller-polyfill/dist/polyfill-patch-fetch';
import Cache from './Cache';

class API {
  /**
   * Clear the local storage
   *
   * @returns {void}
   */
  static clearCache() {
    localStorage.clear();
  }

  /**
   * Build the API object
   *
   * @param {string} url - URL of the endpoint
   * @param {object} options - Optional options
   * @param {object} headers - Optional headers
   * @param {string} method - Optional overwrite of the method
   *
   * @returns {void}
   */
  constructor(url, options = {}, headers = {}, method = '') {
    // Bind parameters
    this.url = url;
    this.headers = headers;
    this.options = options;
    this.method = method;

    // Controller to abort requests
    this.controller = new AbortController();

    // Create cache object
    this.cache = new Cache(url);
    this.cache.lifeTime = options.lifeTime ? options.lifeTime : false;
    this.cache.refreshTimestamp = options.refreshTimestamp
      ? options.refreshTimestamp
      : false;

    // Should response be cached
    this.disableCache = false;
  }

  /**
   * Fetch data from an endpoint
   *
   * @returns {object}
   */
  async fetch() {
    const cachedResult = this.cache.get();

    if (cachedResult) {
      return new Promise((resolve) => {
        resolve(cachedResult);
      });
    }

    // Set headers for init
    const init = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...this.headers,
      },
      signal: this.controller.signal,
    };

    // Determine of post or get
    if (this.method !== '') {
      init.method = this.method;
    } else if (this.options.body) {
      init.method = 'POST';
    } else {
      init.method = 'GET';
    }

    if (this.options.body) {
      init.body = JSON.stringify(this.options.body);
    }

    this.response = await fetch(this.url, init);
    const json = await this.parseJson();

    return json;
  }

  /**
   * Correctly parse the response with headers
   *
   * @returns {object}
   */
  async parseJson() {
    const { response } = this;
    const json = response.json();

    // Disable cache on certain responses
    this.disableCache = (response.status === 404 || response.status === 500);

    // Get exposed headers from response
    const exposeHeaders = response.headers.get('Access-Control-Expose-Headers');
    const headers = exposeHeaders ? exposeHeaders.split(', ') : [];
    const headersObject = headers.reduce((o, key) => ({
      ...o,
      [key]: response.headers.get(key),
    }), {});

    return json.then((data) => ({
      headers: headersObject,
      body: data,
    }));
  }

  /**
   * Put the json in local storage
   *
   * @param {object} json - Data to save
   *
   * @returns {void}
   */
  cachePut(json) {
    if (!this.disableCache) {
      this.cache.put(json);
    }
  }
}

export default API;
